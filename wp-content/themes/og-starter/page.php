<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package og-starter
 */

get_header();
?>

<!--Hero Section Start-->
<?php if (have_rows('hero_section')) : ?>
    <?php while (have_rows('hero_section')) : the_row(); ?>
        <?php
        $hero_background_image = get_sub_field('hero_background_image');
        $hero_top_title = get_sub_field('hero_top_title');
        $hero_main_title = get_sub_field('hero_main_title');
        $hero_subtext = get_sub_field('hero_subtext');
        $hero_cta = get_sub_field('hero_cta');
        $hero_cta_link = get_sub_field('hero_cta_link');
        ?>
        <div id="hero-section" class="hero-grid" style="position:relative;">
            <div class="flex flex-column hero-text">
                <h2 class="ice-blue ttu"><?php echo $hero_top_title; ?></h2>
                <h1 class="ttu"><?php echo $hero_main_title; ?></h1>
                <div class="cta-wrapper desktop-only">
                    <p><?php echo $hero_subtext; ?></p>
                    <a class="cta white orange-bg" href="<?php echo $hero_cta_link['url']; ?>"><?php echo $hero_cta_text; ?></a>
                </div>
            </div>
            <div class="hero-image" style="background:url('<?php echo $hero_background_image['url']; ?>');background-position: top;background-size: cover;background-repeat: no-repeat;">
            </div>
            <img class="hero-mobile-image mobile-only" src="<?php echo $hero_background_image['url']; ?>" />
            <div class="cta-wrapper mobile-only ph4 pb5">
                <p><?php echo $hero_subtext; ?></p>
                <div class="mt4">
                    <a class="cta white orange-bg" href="<?php echo $hero_cta_link['url']; ?>"><?php echo $hero_cta_text; ?></a>
                </div>
            </div>
            <div class="hero-orange-line desktop-only"></div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--Hero Section End-->

<!--Features Section Start-->
<?php if (have_rows('features_section')) : ?>
    <?php while (have_rows('features_section')) : the_row(); ?>
        <?php
        $features_content = get_sub_field('features_content');
        ?>
        <div id="grey-copy-section" style="position:relative" class="light-grey-bg pv3 ph7-l ph4 flex items-center justify-center">
            <p class="dark-grey-2"><?php echo $features_content; ?></p>
            <div class="grey-orange-line desktop-only"></div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--Features Section End-->

<!--Treatment Section Start-->
<?php if (have_rows('treatment_options_section')) : ?>
    <?php while (have_rows('treatment_options_section')) : the_row(); ?>
        <?php
        $side_image = get_sub_field('side_image');
        $treatment_top_title = get_sub_field('treatment_top_title');
        $treatment_main_title = get_sub_field('treatment_main_title');
        $treatment_subtext = get_sub_field('treatment_subtext');
        $treatment_options_left = get_sub_field('treatment_options_left');
        $treatment_options_right = get_sub_field('treatment_options_right');
        $treatment_cta = get_sub_field('treatment_cta');
        $treatment_cta_link = get_sub_field('treatment_cta_link');
        ?>
        <div id="treatment-options-section" class="pa4-l mt5 flex flex-row-l flex-column-reverse">
            <div class="w-50-l w-100 treatment-image" style="background:url('<?php echo $side_image['url']; ?>');background-position: top;background-repeat: no-repeat;background-size: contain;">

            </div>
            <div class="pa0-l pa4 w-50-l w-100 flex flex-column items-center justify-center tc mv5-l second-section-treatment-options">
                <h2 class="ttu self-center-l self-start ice-blue blue-top-header-title"><?php echo $treatment_top_title; ?></h2>
                <h2 class="ttu tc-l tl w-70-l w-100 section-main-title"><?php echo $treatment_main_title; ?></h2>
                <p class="w-70-l w-100 section-paragraph tc-l tl"><?php echo $treatment_subtext; ?></p>
                <div class="w-80-l w-90 flex flex-row-l flex-column justify-between-l">
                    <div class="w-50-l w-100 b mr3 tl flex flex-column justify-end-l items-end-l"><?php echo $treatment_options_left; ?></div>
                    <div class="w-50-l w-100 mt0-l mt3-l b tl flex flex-column justify-end-l items-end-l"><?php echo $treatment_options_right; ?></div>
                </div>
                <div class="mt4 mb0-l mb4">
                    <a class="cta white orange-bg" href="<?php echo $treatment_cta_link['url']; ?>"><?php echo $treatment_cta; ?></a>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--Treatment Section End-->

<!--Credentials Bar Start-->
<div class="light-grey-bg flex flex-row-l flex-column pv4 ph5 justify-between items-center">
    <?php if (have_rows('credentials_bar')) : ?>
        <?php while (have_rows('credentials_bar')) : the_row(); ?>
            <?php
            $icon = get_sub_field('icon');
            $description = get_sub_field('description');
            ?>
            <div class="w-20-l w-100 tc ma4">
                <img src="<?php echo $icon['url']; ?>" />
                <p class="credentials-copy"><?php echo $description; ?></p>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<!--Credentials Bar End-->

<!--Testimonials Section Start-->
<div id="testimonials-section" class="ph0-l ph4 mv5 w-100 flex flex-column items-center justify-center">
    <?php if (have_rows('testimonials')) : ?>
        <?php while (have_rows('testimonials')) : the_row(); ?>
            <?php
            $top_header = get_sub_field('top_header');
            $main_header = get_sub_field('main_header');
            $button_text = get_sub_field('button_text');
            $button_link = get_sub_field('button_link');
            ?>
            <div class="tc">
                <h2 class="ttu ice-blue blue-top-header-title"><?php echo $top_header; ?></h2>
                <h2 class="ttu w-70 section-main-title"><?php echo $main_header; ?></h2>
            </div>

            <div class="w-100 flex ph6-l tl mt4 justify-between testimonial-slider">
                <?php if (have_rows('testimonial_group')) : ?>
                    <?php while (have_rows('testimonial_group')) : the_row(); ?>
                        <?php
                        $name = get_sub_field('name');
                        $image = get_sub_field('image');
                        $quote = get_sub_field('quote');
                        $stars = get_sub_field('stars');
                        $location = get_sub_field('location');
                        $social_icon = get_sub_field('social_icon');
                        $social_link = get_sub_field('social_link');
                        ?>
                        <div class="flex flex-column w-30 testimonial-card-wrapper mb2">
                            <h3 class="orange ttu mb3"><?php echo $name; ?></h3>
                            <img class="mb4 testimonial-image" src="<?php echo $image['url']; ?>" />
                            <p class="testimonial-copy flex mb3">
                                <span class="ice-blue quote mr2">"</span>
                                <span><?php echo $quote; ?></span>
                            </p>
                            <div class="ml4">
                                <img src="<?php echo $stars['url']; ?>" />
                                <div class="flex mt2 items-center">
                                    <h5 class="ma0 mr2 dark-grey"><?php echo $location; ?></h5>
                                    <a href="<?php echo $social_link['url']; ?>"><img src="<?php echo $social_icon['url']; ?>" /></a>
                                </div>

                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="w-100 flex items-center justify-center mt4">
                <div>
                    <a class="cta white orange-bg" href="<?php echo $button_link['url']; ?>"><?php echo $button_text; ?></a>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<!--Testimonials Section End-->

<!--Process Section Start-->
<?php if (have_rows('process_section')) : ?>
    <?php while (have_rows('process_section')) : the_row(); ?>
        <?php
        $top_title = get_sub_field('top_title');
        $main_title = get_sub_field('main_title');
        $button_text = get_sub_field('button_text');
        $button_link = get_sub_field('button_link');
        ?>
        <div id="process-section" class="pv5 w-100 flex flex-column items-center justify-center dark-blue-bg">
            <div class="tc">
                <h2 class="ttu white blue-top-header-title"><?php echo $top_title; ?></h2>
                <h2 class="ttu w-70 section-main-title ice-blue"><?php echo $main_title; ?></h2>
            </div>

            <div class="work-slider w-100">
                <?php if (have_rows('process_card')) : ?>
                    <?php while (have_rows('process_card')) : the_row(); ?>
                        <?php
                        $number = get_sub_field('number');
                        $title_of_process_card = get_sub_field('title_of_process_card');
                        $description_of_process = get_sub_field('description_of_process');
                        ?>
                        <div class="mh3 process-card flex flex-column white-bg w-40 pa4">
                            <div class="flex items-end">
                                <h5 class="number mr4 mv3"><?php echo $number; ?></h5>
                                <h3 class="black process-card-title ma0"><?php echo $title_of_process_card; ?></h3>
                            </div>
                            <p class="mt4 dark-grey mb4 mt0 process-card-styling"><?php echo $description_of_process; ?></p>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="w-100 flex items-center justify-center mt5">
                <div>
                    <a class="cta white orange-bg" style="box-shadow:none;" href="<?php echo $button_link['url']; ?>"><?php echo $button_text; ?></a>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--Process Section End-->

<!--Search Section Start-->
<div id="faq-section" class="row w-100 flex flex-column items-center justify-center mv5">
    <?php if (have_rows('search_section')) : ?>
        <?php while (have_rows('search_section')) : the_row(); ?>
            <?php
            $top_title = get_sub_field('top_title');
            $main_title = get_sub_field('main_title');
            $description_under_title = get_sub_field('description_under_title');
            $search_placeholder_text = get_sub_field('search_placeholder_text');
            $copy_under_search_bar = get_sub_field('copy_under_search_bar');
            ?>
            <div class="tc mb4-l flex flex-column items-center">
                <h2 class="ttu ice-blue blue-top-header-title"><?php echo $top_title; ?></h2>
                <h2 class="ttu section-main-title"><?php echo $main_title; ?></h2>
                <p class="w-40-l w-70 faq-description"> <?php echo $description_under_title; ?></p>
            </div>
            <!--Start of Search Bar-->

            <div class="w-60-l w-80 ml3 flex justify-center quick-search-copy">
                <?php echo $copy_under_search_bar; ?>
            </div>
            <!--End of Search Bar-->
            <!--Start of Keywords-->
            <div class="flex flex-row-l flex-wrap items-center justify-between mb4 mt0 ph0-l ph5">
                <?php if (have_rows('keywords')) : ?>
                    <?php while (have_rows('keywords')) : the_row(); ?>
                        <?php
                        $key_word_title  = get_sub_field('key_word_title');
                        $key_word_placement = get_sub_field('key_word_placement');
                        ?>
                        <div class="keyword-styling ma2 tc">
                            <button onclick="keyWordClick(<?php echo $key_word_placement; ?>);" class="keyWordInside">
                                <?php echo $key_word_title; ?>
                            </button>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="flex items-center w-20-l w-50 ma0 tc mb3 justify-between no-show" id="filterwrapper">
                <div>
                    <p class="faq-description">Filter applied for <span id="filterwrapper-inside" style="font-style:italic;"></span>.</p>
                </div>
                <div>
                    <button class="button-remove-filter" onclick="removeFilter();"> x</button>
                </div>
            </div>
            <!--End of Keywords-->
            <!--Start of Cards-->
            <div class="w-100 flex flex-wrap justify-center items-center" id="wrapperFull">
                <?php if (have_rows('card_symptoms')) : ?>
                    <?php while (have_rows('card_symptoms')) : the_row(); ?>
                        <?php
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $keyword_1 = get_sub_field('keyword_1');
                        $keyword_2 = get_sub_field('keyword_2');
                        ?>
                        <div class="flex w-40-l w-90 ma3 pa4 card">
                            <div class="flex flex-column justify-between">
                                <h2 class="search-title orange"><?php echo $title; ?></h2>
                                <p class="card-description"><?php echo $description; ?></p>
                                <div class="flex">
                                    <button class="mr3 keyword-oncard-styling-2" disabled><?php echo $keyword_1; ?></button>
                                    <button class="keyword-oncard-styling-2" disabled><?php echo $keyword_2; ?></button>
                                </div>
                            </div>
                            <div class="no-show" id="xClick">x</div>
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="w-100 mv3-l ph7-l ma4 ph3 tc no-show" id="sorry-message">
                <h2 style="font-style:italic">Sorry, the keywords you are looking for are not pulling up any information, please try again.</h2>
            </div>
            <!--End of Cards-->
            <!--Start of FAQ Mobile Section-->
            <div class="mobile-only mh4" style="display:none;">
                <?php if (have_rows('faq_questions')) : ?>
                    <?php while (have_rows('faq_questions')) : the_row(); ?>
                        <?php
                        $question = get_sub_field('question');
                        $answer = get_sub_field('answer');
                        ?>
                        <button class="accordion"><?php echo $question; ?></button>
                        <div class="panel">
                            <p><?php echo $answer; ?></p>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <!--End of FAQ Mobile Section-->

        <?php endwhile; ?>
    <?php endif; ?>
</div>
<!--End of New Search Section -->

<!--About Clinic Section-->
<?php if (have_rows('about_clinic_section')) : ?>
    <?php while (have_rows('about_clinic_section')) : the_row(); ?>
        <?php
        $background_image = get_sub_field('background_image');
        $top_title = get_sub_field('top_title');
        $main_title = get_sub_field('main_title');
        $about_clinic_paragraph = get_sub_field('about_clinic_paragraph');
        ?>
        <div id="about-clinic-section" class="flex flex-row-l flex-column w-100 pv5 ph5-l ph4 justify-between" style="background:url('<?php echo $background_image['url']; ?>');background-repeat: no-repeat;background-size: cover;">
            <div class="mh4-l w-50-l w-100 flex flex-column items-center justify-center">
                <h2 class="blue-top-header-title orange ttu"><?php echo $top_title; ?></h2>
                <h2 class="section-main-title ttu white tc"><?php echo $main_title; ?></h2>
                <p class="white tc about-clinic-paragraph w-70-l w-90"><?php echo $about_clinic_paragraph; ?></p>
            </div>
            <div class="mh4-l w-50-l w-100 flex flex-wrap about-clinic-facts-wrap">
                <?php if (have_rows('facts_about_clinic')) : ?>
                    <?php while (have_rows('facts_about_clinic')) : the_row(); ?>
                        <?php
                        $facts = get_sub_field('facts');
                        ?>
                        <div class="w-40-l w-100 mh3-l pa2 about-facts">
                            <p class="white about-clinic-facts"><?php echo $facts; ?></p>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--End of About Clinic Section-->

<!--Start of Form Section-->
<?php if (have_rows('form_section')) : ?>
    <?php while (have_rows('form_section')) : the_row(); ?>
        <?php
        $title = get_sub_field('title');
        $copy_under_title = get_sub_field('copy_under_title');
        $form_field = get_sub_field('form_field');
        ?>
        <div id="discuss" class="justify-center mt5 mb4 w-100 flex items-center"><span class="vertical-grey-line-form dark-grey-bg"></span></div>
        <div class="ph4 flex flex-column w-100 items-center">
            <h2 class="section-main-title ttu black"><?php echo $title; ?></h2>
            <p class="form-copy tc mb5"><?php echo $copy_under_title; ?></p>
            <div id="regular" class="mb6">
                <?php if ($form_field != '') {
                    echo '<div class="w-100">' . $form_field .  '</div>';
                } else {
                    echo   do_shortcode('[contact-form-7 id="14" title="Main Page Form"]');
                }; ?>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--End of Form Section-->


<script>
    //Checking with Buttons
    function keyWordClick(x) {
        var keyWordInside = document.getElementsByClassName('keyWordInside');
        var keyWord = keyWordInside[x].innerText;
        var upperKeyWord = keyWord.toUpperCase();
        const m = document.getElementsByClassName('card');
        for (var i = 0; i < m.length; i++) {
            const keyWords = m[i].innerText;
            if (keyWords.toUpperCase().indexOf(upperKeyWord) > -1) {
                m[i].style.display = 'block';
                document.getElementById('filterwrapper').classList.remove('no-show');
                document.getElementById('filterwrapper-inside').innerText = keyWord;

            } else {
                m[i].style.display = 'none';

            }
        };
    };

    function removeFilter() {
        var cardShow = document.getElementsByClassName('card');
        for (i = 0; i < cardShow.length; i++) {
            cardShow[i].style.display = 'block'; //shows all cards again
            document.getElementById('filterwrapper').classList.add('no-show');

        }
    }
</script>

<script>
    //START OF SLIDER FOR PROCESS AND TESTIMONIALS
    //PROCESS SLIDER
    $(document).ready(function() {
        $('.work-slider').slick({
            centerMode: true,
            centerTop: '0px',
            index: 2,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            responsive: [{
                breakpoint: 960,
                settings: {
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }]
        });
        //TESTIMONIAL SLIDER
        if ($(window).width() < 960) {
            $('.testimonial-slider').slick({
                dots: true,
                infinite: true,
                slidestoShow: 1,
                arrows: false
            });
        } else {

        }
    });
    //END OF SLIDER FOR PROCESS AND TESTIMONIALS

    /*START OF FORM STYLING */
    $("#FirstName").keyup(function() {
        var input1 = document.getElementById('FirstName');
        var fnamelabel = document.getElementById('fnamelabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            fnamelabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            fnamelabel.classList.remove('active-field-input');
        }
    });
    $("#LastName").keyup(function() {
        var input1 = document.getElementById('LastName');
        var lnamelabel = document.getElementById('lnamelabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            lnamelabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            lnamelabel.classList.remove('active-field-input');
        }
    });
    $("#Email").keyup(function() {
        var input1 = document.getElementById('Email');
        var emailLabel = document.getElementById('emailLabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            emailLabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            emailLabel.classList.remove('active-field-input');
        }
    });
    $("#Phone").keyup(function() {
        var input1 = document.getElementById('Phone');
        var phonelabel = document.getElementById('phonelabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            phonelabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            phonelabel.classList.remove('active-field-input');
        }
    });
    $("#FollowUp").keyup(function() {
        var input1 = document.getElementById('FollowUp');
        var followuplabel = document.getElementById('followuplabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            followuplabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            followuplabel.classList.remove('active-field-input');
        }
    });
    $("#ContactTime").keyup(function() {
        var input1 = document.getElementById('ContactTime');
        var contacttimelabel = document.getElementById('contacttimelabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            contacttimelabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            contacttimelabel.classList.remove('active-field-input');
        }
    });
    /*END OF FORM STYLING */
</script>




<?php
get_footer();
