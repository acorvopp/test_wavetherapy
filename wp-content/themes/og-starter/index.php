<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package og-starter
 */

get_header();
?>

<!--Hero Section Start-->
<?php if (have_rows('hero_section')) : ?>
	<?php while (have_rows('hero_section')) : the_row(); ?>
		<?php
		$hero_background_image = get_sub_field('hero_background_image');
		$hero_top_title = get_sub_field('hero_top_title');
		$hero_main_title = get_sub_field('hero_main_title');
		$hero_subtext = get_sub_field('hero_subtext');
		$hero_cta = get_sub_field('hero_cta');
		$hero_cta_link = get_sub_field('hero_cta_link');
		?>
		<div id="hero-section" class="hero-grid" style="position:relative;">
			<div class="flex flex-column hero-text">
				<h2 class="ice-blue ttu"><?php echo $hero_top_title; ?></h2>
				<h1 class="ttu"><?php echo $hero_main_title; ?></h1>
				<div class="cta-wrapper desktop-only">
					<p><?php echo $hero_subtext; ?></p>
					<a class="cta white orange-bg" href="<?php echo $hero_cta_link['url']; ?>"><?php echo $hero_cta_text; ?></a>
				</div>
			</div>
			<div class="hero-image" style="background:url('<?php echo $hero_background_image['url']; ?>');background-position: top;background-size: cover;background-repeat: no-repeat;">
			</div>
			<img class="hero-mobile-image mobile-only" src="<?php echo $hero_background_image['url']; ?>" />
			<div class="cta-wrapper mobile-only ph4 pb5">
				<p><?php echo $hero_subtext; ?></p>
				<div class="mt4">
					<a class="cta white orange-bg" href="<?php echo $hero_cta_link['url']; ?>"><?php echo $hero_cta_text; ?></a>
				</div>
			</div>
			<div class="hero-orange-line desktop-only"></div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
<!--Hero Section End-->

<!--Start of Grey Copy Section-->
<?php if (have_rows('features_section')) : ?>
	<?php while (have_rows('features_section')) : the_row(); ?>
		<?php
		$features_content = get_sub_field('features_content');
		?>
		<div id="grey-copy-section" style="position:relative" class="light-grey-bg pv3 ph7-l ph4 flex items-center justify-center">
			<p class="dark-grey-2"><?php echo $features_content; ?></p>
			<div class="grey-orange-line desktop-only"></div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
<!--Start of Grey Copy Section-->

<!--Start of Treatment Options-->
<?php if (have_rows('treatment_options_section')) : ?>
	<?php while (have_rows('treatment_options_section')) : the_row(); ?>
		<?php
		$side_image = get_sub_field('side_image');
		$treatment_top_title = get_sub_field('treatment_top_title');
		$treatment_main_title = get_sub_field('treatment_main_title');
		$treatment_subtext = get_sub_field('treatment_subtext');
		$treatment_options_left = get_sub_field('treatment_options_left');
		$treatment_options_right = get_sub_field('treatment_options_right');
		$treatment_cta = get_sub_field('treatment_cta');
		$treatment_cta_link = get_sub_field('treatment_cta_link');
		?>
		<div id="treatment-options-section" class="pa4-l mt5 flex flex-row-l flex-column-reverse">
			<div class="w-50-l w-100 treatment-image" style="background:url('<?php echo $side_image['url']; ?>');background-position: top;background-repeat: no-repeat;background-size: contain;">

			</div>
			<div class="pa0-l pa4 w-50-l w-100 flex flex-column items-center justify-center tc mv5-l second-section-treatment-options">
				<h2 class="ttu self-center-l self-start ice-blue blue-top-header-title"><?php echo $treatment_top_title; ?></h2>
				<h2 class="ttu tc-l tl w-70-l w-100 section-main-title"><?php echo $treatment_main_title; ?></h2>
				<p class="w-70-l w-100 section-paragraph tc-l tl"><?php echo $treatment_subtext; ?></p>
				<div class="w-80-l w-90 flex flex-row-l flex-column justify-between-l">
					<div class="w-50-l w-100 b mr3 tl flex flex-column justify-end-l items-end-l"><?php echo $treatment_options_left; ?></div>
					<div class="w-50-l w-100 mt0-l mt3-l b tl flex flex-column justify-end-l items-end-l"><?php echo $treatment_options_right; ?></div>
				</div>
				<div class="mt4 mb0-l mb4">
					<a class="cta white orange-bg" href="<?php echo $treatment_cta_link['url']; ?>"><?php echo $treatment_cta; ?></a>
				</div>
			</div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
<!--End of Treatment Options-->


<?php
get_footer();
