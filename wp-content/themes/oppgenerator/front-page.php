<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package og-starter
 */

get_header();
?>

<!--Hero Section Start-->
<?php if (have_rows('hero_section')) : ?>
    <?php while (have_rows('hero_section')) : the_row(); ?>
        <?php
        $hero_background_image = get_sub_field('hero_background_image');
        $hero_top_title = get_sub_field('hero_top_title');
        $hero_main_title = get_sub_field('hero_main_title');
        $hero_subtext = get_sub_field('hero_subtext');
        $hero_cta = get_sub_field('hero_cta');
        $hero_cta_link = get_sub_field('hero_cta_link');
        ?>
        <!--Hero Content-->
        <div id="hero-section" class=" w-100 flex flex-row-l flex-column">
            <div class="pl5-l w-50-l w-100 flex flex-column hero-text" style="position:relative;">
                <h3 class="ttu ice-blue blue-top-header-title"><?php echo $hero_top_title; ?></h3>
                <h1 class="ttu"><?php echo $hero_main_title; ?></h1>
                <div class="cta-wrapper desktop-only">
                    <p><?php echo $hero_subtext; ?></p>
                    <div class="flex flex-column items-start">
                        <a class="cta white orange-bg" href="<?php echo $hero_cta_link['url']; ?>"><?php echo $hero_cta; ?></a>
                        <a class="link mt3 orange link-cta-hover" href="tel:<?php the_field('mobile_phone_cta_number', 'options'); ?>"><?php the_field('phone_cta_text', 'options'); ?> <span> <?php the_field('mobile_phone_cta_number', 'options'); ?></span></a>
                    </div>
                </div>
            </div>
            <div class="w-50-l w-100 hero-main-image" style="background:url('<?php echo $hero_background_image['url']; ?>');background-position: top left 25%;background-size: 90%;background-repeat: no-repeat; background-size:cover; height:auto;"></div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--Hero Section End-->

<!--Features Section Start-->
<div class="light-grey-bg flex flex-row-l flex-column pv4 ph5 justify-between items-center">
    <?php if (have_rows('features_section')) : ?>
        <?php while (have_rows('features_section')) : the_row(); ?>
            <?php
            $feature_icon = get_sub_field('feature_icon');
            $feature_description = get_sub_field('feature_description');
            ?>
            <div class="w-20-l w-100 tc ma4">
                <img src="<?php echo $feature_icon['url']; ?>" />
                <p class="credentials-copy"><?php echo $feature_description; ?></p>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<!--Feaatures Section End-->

<!--Treatment Section Start-->
<?php if (have_rows('treatment_options_section')) : ?>
    <?php while (have_rows('treatment_options_section')) : the_row(); ?>
        <?php
        $side_image = get_sub_field('side_image');
        $treatment_top_title = get_sub_field('treatment_top_title');
        $treatment_main_title = get_sub_field('treatment_main_title');
        $treatment_subtext = get_sub_field('treatment_subtext');
        $treatment_options_left = get_sub_field('treatment_options_left');
        $treatment_options_right = get_sub_field('treatment_options_right');
        $treatment_cta = get_sub_field('treatment_cta');
        $treatment_cta_link = get_sub_field('treatment_cta_link');
        ?>
        <div id="treatment-options-section" class="pa4-l mt5 flex flex-row-l flex-column-reverse">
            <div class="w-50-l w-100 treatment-image" style="background:url('<?php echo $side_image['url']; ?>');background-position: top;background-repeat: no-repeat;background-size: contain;">

            </div>
            <div class="pa0-l pa4 w-50-l w-100 flex flex-column items-center justify-center tc mv5-l second-section-treatment-options">
                <h3 class="ttu self-center-l ice-blue blue-top-header-title"><?php echo $treatment_top_title; ?></h3>
                <h2 class="ttu tc-l tl w-70-l w-100 section-main-title"><?php echo $treatment_main_title; ?></h2>
                <p class="w-70-l w-100 section-paragraph tc-l tl"><?php echo $treatment_subtext; ?></p>
                <div class="w-80-l w-90 flex flex-row-l flex-column justify-between-l">
                    <div class="w-50-l w-100 b mr3 tl flex flex-column justify-end-l items-end-l"><?php echo $treatment_options_left; ?></div>
                    <div class="w-50-l w-100 mt0-l mt3-l b tl flex flex-column justify-end-l items-end-l"><?php echo $treatment_options_right; ?></div>
                </div>
                <div class="mt4 mb0-l mb4">
                    <a class="cta white orange-bg" href="<?php echo $treatment_cta_link['url']; ?>"><?php echo $treatment_cta; ?></a>
                </div>
                <a class="link mt3 orange link-cta-hover" href="tel:<?php the_field('mobile_phone_cta_number', 'options'); ?>"><?php the_field('phone_cta_text', 'options'); ?> <span> <?php the_field('mobile_phone_cta_number', 'options'); ?></span></a>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--Treatment Section End-->

<!--Testimonials Section Start-->
<div id="testimonials-section" class="ph0-l ph4 w-100 flex flex-column items-center justify-center">
    <?php if (have_rows('testimonials')) : ?>
        <?php while (have_rows('testimonials')) : the_row(); ?>
            <?php
            $testimonials_top_title = get_sub_field('testimonials_top_title');
            $testimonials_main_title = get_sub_field('testimonials_main_title');
            $testimonials_cta = get_sub_field('testimonials_cta');
            $testimonials_cta_link = get_sub_field('testimonials_cta_link');
            ?>
            <div class="tc">
                <h3 class="ttu ice-blue blue-top-header-title"><?php echo $testimonials_top_title; ?></h3>
                <h2 class="ttu section-main-title"><?php echo $testimonials_main_title; ?></h2>
            </div>

            <div class="grid">
                <?php if (have_rows('testimonials_group')) : ?>
                    <?php while (have_rows('testimonials_group')) : the_row(); ?>
                        <?php
                        $name = get_sub_field('name');
                        $image = get_sub_field('image');
                        $quote = get_sub_field('quote');
                        ?>
                        <div class="w-100 mb2">
                            <h3 class="orange ttu mb3"><?php echo $name; ?></h3>
                            <img class="mb4 testimonial-image" src="<?php echo $image['url']; ?>" />
                            <p class="testimonial-copy mb3">
                                <span class="ice-blue quote mr2">"</span>
                                <span><?php echo $quote; ?></span>
                            </p>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <div class="w-100 pb4 flex items-center justify-center mt5">
                <div class="flex flex-column items-center justify-center">
                    <a class="cta white orange-bg" style="box-shadow:none;" href="<?php echo $testimonials_cta_link['url']; ?>"><?php echo $testimonials_cta; ?></a>

                    <a class="link mt3 orange link-cta-hover" href="tel:<?php the_field('mobile_phone_cta_number', 'options'); ?>"><?php the_field('phone_cta_text', 'options'); ?> <span> <?php the_field('mobile_phone_cta_number', 'options'); ?></span></a>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<!--Testimonials Section End-->

<!--Process Section Start-->
<?php if (have_rows('process_section')) : ?>
    <?php while (have_rows('process_section')) : the_row(); ?>
        <?php
        $process_top_title = get_sub_field('process_top_title');
        $process_main_title = get_sub_field('process_main_title');
        $process_cta = get_sub_field('process_cta');
        $process_cta_link = get_sub_field('process_cta_link');
        ?>
        <div id="process-section" class="pv5 w-100 flex flex-column items-center justify-center dark-blue-bg">
            <div class="tc">
                <h3 class="ttu white blue-top-header-title"><?php echo $process_top_title; ?></h3>
                <h2 class="ttu section-main-title ice-blue"><?php echo $process_main_title; ?></h2>
            </div>

            <div class="grid">
                <?php if (have_rows('process_card')) : ?>
                    <?php while (have_rows('process_card')) : the_row(); ?>
                        <?php
                        $process_step = get_sub_field('process_step');
                        $process_title = get_sub_field('process_title');
                        $process_description = get_sub_field('process_description');
                        ?>
                        <div class="process-card white-bg w-100 pa4">
                            <div>
                                <h5 class="number mr4 mv3"><?php echo $process_step; ?></h5>
                            </div>
                            <div>
                                <h3 class="mt2 black process-card-title ma0"><?php echo $process_title; ?></h3>
                            </div>
                            <div>
                                <p class="mt4 dark-grey mb4 mt0 process-card-styling"><?php echo $process_description; ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="w-100 flex items-center justify-center mt5">
                <div class="flex flex-column items-center justify-center">
                    <a class="cta white orange-bg" style="box-shadow:none;" href="<?php echo $process_cta_link['url']; ?>"><?php echo $process_cta; ?></a>

                    <a class="link mt3 orange link-cta-hover" href="tel:<?php the_field('mobile_phone_cta_number', 'options'); ?>"><?php the_field('phone_cta_text', 'options'); ?> <span> <?php the_field('mobile_phone_cta_number', 'options'); ?></span></a>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--Process Section End-->

<!--FAQ Section Start-->
<div id="faq-section" class="row w-100 flex flex-column items-center justify-center mv5">
    <?php if (have_rows('faq_section')) : ?>
        <?php while (have_rows('faq_section')) : the_row(); ?>
            <?php
            $faq_top_title = get_sub_field('faq_top_title');
            $faq_main_title = get_sub_field('faq_main_title');
            $faq_subtext = get_sub_field('faq_subtext');
            ?>
            <div class="tc mb4-l flex flex-column items-center">
                <h3 class="ttu ice-blue blue-top-header-title"><?php echo $faq_top_title; ?></h3>
                <h2 class="ttu section-main-title"><?php echo $faq_main_title; ?></h2>
                <p class="w-40-l w-70 faq-description"> <?php echo $faq_subtext; ?></p>
            </div>
            <!--Filter Start-->
            <div class="flex flex-row-l flex-wrap items-center justify-between mb4 mt0 ph0-l ph5">
                <?php if (have_rows('keywords')) : ?>
                    <?php while (have_rows('keywords')) : the_row(); ?>
                        <?php
                        $faq_key_word_title  = get_sub_field('key_word_title');
                        $faq_key_word_placement = get_sub_field('key_word_placement');
                        ?>
                        <div class="keyword-styling ma2 tc">
                            <button onclick="keyWordClick(<?php echo $faq_key_word_placement; ?>);" class="keyWordInside">
                                <?php echo $faq_key_word_title; ?>
                            </button>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <!--Filter End-->
            <div class="flex items-center w-20-l w-50 ma0 tc mb3 justify-between no-show" id="filterwrapper">
                <div>
                    <p class="faq-description">Filter applied for <span id="filterwrapper-inside" style="font-style:italic;"></span>.</p>
                </div>
                <div>
                    <button class="button-remove-filter" onclick="removeFilter();"> x</button>
                </div>
            </div>
            <!--Filter End-->
            <!--FAQ Card Block Start-->
            <div class="w-100 flex flex-wrap justify-center items-center" id="wrapperFull">
                <?php if (have_rows('card_symptoms')) : ?>
                    <?php while (have_rows('card_symptoms')) : the_row(); ?>
                        <?php
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $keyword_one = get_sub_field('keyword_one');
                        $keyword_two = get_sub_field('keyword_two');
                        ?>
                        <div class="flex w-40-l w-90 ma3 pa4 card">
                            <div class="flex flex-column justify-between">
                                <h3 class="search-title orange"><?php echo $title; ?></h3>
                                <p class="card-description"><?php echo $description; ?></p>
                                <div class="flex">
                                    <button class="mr3 keyword-oncard-styling-2" disabled><?php echo $keyword_one; ?></button>
                                    <button class="keyword-oncard-styling-2" disabled><?php echo $keyword_two; ?></button>
                                </div>
                            </div>
                            <div class="no-show" id="xClick">x</div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <div class="mobile-only mh4" style="display:none;">
                <?php if (have_rows('faq_questions')) : ?>
                    <?php while (have_rows('faq_questions')) : the_row(); ?>
                        <?php
                        $question = get_sub_field('question');
                        $answer = get_sub_field('answer');
                        ?>
                        <button class="accordion"><?php echo $question; ?></button>
                        <div class="panel">
                            <p><?php echo $answer; ?></p>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
            <!--FAQ Mobile Section End-->
        <?php endwhile; ?>
    <?php endif; ?>
</div>
<!--FAQ Section End-->

<!--About Section Start-->
<?php if (have_rows('about_section')) : ?>
    <?php while (have_rows('about_section')) : the_row(); ?>
        <?php
        $background_image = get_sub_field('background_image');
        $about_top_title = get_sub_field('about_top_title');
        $about_main_title = get_sub_field('about_main_title');
        $about_subtext = get_sub_field('about_subtext');
        ?>
        <div id="about-clinic-section" class="flex flex-row-l flex-column w-100 pv5 ph5-l ph4 justify-between" style="background:url('<?php echo $background_image['url']; ?>');background-repeat: no-repeat;background-size: cover;">
            <div class="mh4-l w-50-l w-100 flex flex-column items-center justify-center">
                <h3 class="blue-top-header-title orange ttu"><?php echo $about_top_title; ?></h3>
                <h2 class="section-main-title ttu white tc"><?php echo $about_main_title; ?></h2>
                <p class="white tc about-clinic-paragraph w-70-l w-90"><?php echo $about_subtext; ?></p>
            </div>
            <div class="mh4-l w-50-l w-100 flex flex-wrap about-clinic-facts-wrap">
                <?php if (have_rows('about_points')) : ?>
                    <?php while (have_rows('about_points')) : the_row(); ?>
                        <?php
                        $points = get_sub_field('points');
                        ?>
                        <div class="w-40-l w-100 mh3-l pa2 about-facts">
                            <p class="white about-clinic-facts"><?php echo $points; ?></p>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--About Section End-->

<!--Form Section Start-->
<?php if (have_rows('form_section')) : ?>
    <?php while (have_rows('form_section')) : the_row(); ?>
        <?php
        $form_top_title = get_sub_field('form_top_title');
        $form_main_title = get_sub_field('form_main_title');
        $form_subtext = get_sub_field('form_subtext');
        $jotform_regular = get_sub_field('jotform_regular');
        ?>
        <div id="discuss" class="justify-center mt5 mb4 w-100 flex items-center"><span class="vertical-grey-line-form dark-grey-bg"></span></div>
        <div class="ph4 flex flex-column w-100 items-center">
            <h3 class="ttu ice-blue blue-top-header-title"><?php echo $form_top_title; ?></h3>
            <h2 class="section-main-title ttu black"><?php echo $form_main_title; ?></h2>
            <p class="form-copy tc mb5"><?php echo $form_subtext; ?></p>
            <div class="mt4 w-100 flex items-center justify-center" id="regular">
                <?php
                if ($jotform_regular != '') {
                    echo  '<div class="w-100" style="margin-top:-100px;">' .  $jotform_regular . '</div>';
                } else {
                    echo do_shortcode('[contact-form-7 id="819" title="Alt Page Form"]');
                }
                ?>
            </div>
        </div>
    <?php endwhile; ?>
<?php endif; ?>
<!--Form Section End-->

<!--Cookie Banner Start-->
<div id="cookie-banner-id" class="cookie-banner flex flex-row-l flex-column items-center justify-center ph6-l ph4 pv4-l pv5">
    <h2 class="cookie-banner-title w-30-l w-100 tc ttu ma0 mb0-l mb3"><?php the_field('main_title', 'options'); ?></h2>
    <p class="w-50-l w-100 ma0 pa0 mt0-l mt2 tl-l tc"><?php the_field('copy', 'options'); ?></p>
    <button class="close-popup-banner" onclick="closeBanner();"></button>
</div>
<!--Cookie Banner End-->



<script>
    if (localStorage.getItem('popUpBanner')) {
        document.querySelector('#cookie-banner-id').style.display = 'none';
    }

    function closeBanner() {
        var popup = document.querySelector('#cookie-banner-id');
        popup.style.display = 'none';
        localStorage.setItem('popUpBanner', true);
    }


    function keyWordClick(x) {
        var keyWordInside = document.getElementsByClassName('keyWordInside');
        var keyWord = keyWordInside[x].innerText;
        var upperKeyWord = keyWord.toUpperCase();
        const m = document.getElementsByClassName('card');
        for (var i = 0; i < m.length; i++) {
            const keyWords = m[i].innerText;
            if (keyWords.toUpperCase().indexOf(upperKeyWord) > -1) {
                m[i].style.display = 'block';
                document.getElementById('filterwrapper').classList.remove('no-show');
                document.getElementById('filterwrapper-inside').innerText = keyWord;
            } else {
                m[i].style.display = 'none';
            }
        };
    };

    function removeFilter() {
        var cardShow = document.getElementsByClassName('card');
        for (i = 0; i < cardShow.length; i++) {
            cardShow[i].style.display = 'block'; //shows all cards again
            document.getElementById('filterwrapper').classList.add('no-show');

        }
    }

    $("#FirstName").keyup(function() {
        var input1 = document.getElementById('FirstName');
        var fnamelabel = document.getElementById('fnamelabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            fnamelabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            fnamelabel.classList.remove('active-field-input');
        }
    });
    $("#LastName").keyup(function() {
        var input1 = document.getElementById('LastName');
        var lnamelabel = document.getElementById('lnamelabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            lnamelabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            lnamelabel.classList.remove('active-field-input');
        }
    });
    $("#Email").keyup(function() {
        var input1 = document.getElementById('Email');
        var emailLabel = document.getElementById('emailLabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            emailLabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            emailLabel.classList.remove('active-field-input');
        }
    });
    $("#Phone").keyup(function() {
        var input1 = document.getElementById('Phone');
        var phonelabel = document.getElementById('phonelabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            phonelabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            phonelabel.classList.remove('active-field-input');
        }
    });
    $("#FollowUp").keyup(function() {
        var input1 = document.getElementById('FollowUp');
        var followuplabel = document.getElementById('followuplabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            followuplabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            followuplabel.classList.remove('active-field-input');
        }
    });
    $("#ContactTime").keyup(function() {
        var input1 = document.getElementById('ContactTime');
        var contacttimelabel = document.getElementById('contacttimelabel');
        if (input1.value.length > 0) {
            input1.style.background = 'white';
            contacttimelabel.classList.add('active-field-input');
        } else {
            input1.style.background = '#fff0';
            contacttimelabel.classList.remove('active-field-input');
        }
    });
</script>

<?php
get_footer();
