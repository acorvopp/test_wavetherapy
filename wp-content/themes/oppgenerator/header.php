<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="stylesheet" href="https://unpkg.com/tachyons@4.10.0/css/tachyons.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div id="navigation-wrapper">
        <header id="myHeader">
            <div class="mv4-l mv3 ph5-l ph4 w-100 flex justify-between items-center top-menu-header">
                <div class="w-20-l W-70">
                    <a href="/" aria-label="Click here to go to the homepage.">
                        <img class="logo-navigation" src="<?php the_field('logo', 'option'); ?>" alt="Company Logo name goes here." />
                    </a>
                </div>
                <div id="myTopnav" class="topnav">
                    <nav id="site-navigation">
                        <?php
                        wp_nav_menu(array(
                            'theme_location' => 'menu-1',
                            'menu_id'        => 'main-nav',
                            'menu_class' => 'menu-main-container',
                        ));
                        ?>
                    </nav>
                </div>
                <div class="w-20 bottom-cta-fixed">
                    <a class="cta-left w-50" href="#discuss">
                        <?php the_field('navigation_cta', 'options') ?>
                    </a>
                    <a class="cta-right w-50 mobile-only dark-grey-bg white" href="tel:<?php the_field('mobile_phone_cta_number', 'options') ?>">
                        <?php the_field('mobile_phone_cta_text', 'options') ?>
                    </a>
                </div>
                <div class="w-10 mobile-only">
                    <button class="mobile-button" onclick="myFunction();">
                        <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 24 24" fill="none" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-align-left">
                            <line x1="17" y1="10" x2="3" y2="10"></line>
                            <line x1="21" y1="6" x2="3" y2="6"></line>
                            <line x1="21" y1="14" x2="3" y2="14"></line>
                            <line x1="17" y1="18" x2="3" y2="18"></line>
                        </svg>
                    </button>
                </div>
            </div>
        </header>
    </div>

    <div id="page" class="site">
        <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'og-starter'); ?></a>
        <div id="content" class="site-content">