<footer class="w-100 flex flex-column dark-blue-bg">
    <iframe id="googleMap" src="<?php the_field('map_url', 'option'); ?>" width="100%" height="400" frameborder="0" style="border:0;pointer-events: none;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    <?php if (have_rows('contact_information', 'options')) : ?>
        <?php while (have_rows('contact_information', 'options')) : the_row(); ?>
            <?php
            $title_one = get_sub_field('title_one');
            $phone_number = get_sub_field('phone_number');
            $address = get_sub_field('address');
            $title_two = get_sub_field('title_two');
            $description = get_sub_field('description_paragraph');
            $logo = get_sub_field('logo');
            ?>
            <div class="ph5-l pv4-l pa4 justify-between-l items-start-l items-center flex flex-row-l w-100 flex-column justify-center">
                <div class="w-20-l w-100 flex flex-column">
                    <h2 class="white ttu"><?php echo $title_one; ?></h2>
                    <div class="flex mv2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="17px" height="24" viewBox="0 0 24 24" fill="none" stroke="#EF805D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone mr3">
                            <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                        </svg>
                        <a class="link" href="tel:<?php echo $phone_number; ?>">
                            <p class="white contact-details ma0"><?php echo $phone_number; ?></p>
                        </a>
                    </div>
                    <div class="flex">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#EF805D" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-map-pin mr3">
                            <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
                            <circle cx="12" cy="10" r="3"></circle>
                        </svg>
                        <p class="white contact-details ma0"><?php echo $address; ?></p>
                    </div>
                </div>
                <div class="w-40-l w-100 flex flex-column">
                    <h2 class="white ttu"><?php echo $title_two; ?></h2>
                    <p class="white contact-description"><?php echo $description; ?></p>
                </div>
                <div class="mt5 w-20-l w-100 flex items-center justify-center logo-border-bottom">
                    <img class="footer-image" src="<?php echo $logo['url']; ?>" />
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
    <?php if (have_rows('details_footer', 'options')) : ?>
        <?php while (have_rows('details_footer', 'options')) : the_row(); ?>
            <?php
            $designed = get_sub_field('designed');
            $privacy_policy = get_sub_field('privacy_policy');
            $privacy_policy_link = get_sub_field('privacy_policy_link');
            $cookies_policy = get_sub_field('cookies_policy');
            $cookies_policy_link = get_sub_field('cookies_policy_link');
            ?>
            <div class="flex ph4-l ph3 white mb3-l mb5 items-center">
                <h4 class="mh2 mt0 mb0 pa0 detail-footer"><?php echo $designed; ?></h4> |
                <a class="mh2 detail-footer white link" target="_blank" href="<?php echo $privacy_policy_link['url']; ?>">
                    <?php echo $privacy_policy; ?>
                </a> |
                <a class="mh2 detail-footer white link" target="_blank" href="<?php echo $cookies_policy_link['url']; ?>">
                    <?php echo $cookies_policy; ?>
                </a>
                <p class="detail-footer">* Some images used are stock photography and not actual patients.</p>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
</footer>

<script>
    // Filter Faqs
    var acc = document.getElementsByClassName("accordion");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    }
</script>
</div>

<?php wp_footer(); ?>

</body>

</html>